import React, { Component } from 'react';
import CommandPane from './CommandPane';
import TaskTable from './TaskTable';
import TaskCard from './TaskCard';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            statuses: [
                { name: 'To Do' },
                { name: 'Need More Info' },
                { name: 'In Progress' },
                { name: 'Ready For Testing' },
                { name: 'Testing'}, 
                { name: 'Done' }
            ],

            tasks: [
                {
                    id: 'RCT-1',
                    name: 'Complete React tutorial',
                    description: 'Tuturial <a href="https://reactjs.org/tutorial/tutorial.html">here</a>',
                    assigner: 'Aleksey Gotyanov',
                    statusName: 'To Do'
                },
                {
                    id: 'RTC-2',
                    name: 'Read React docs',
                    description: 'do it',
                    assigner: 'Aleksey Gotyanov',
                    statusName: 'To Do'
                }
            ]
        }
    }

    render() {
        let state = this.state;

        let columns = state.statuses.map(status => ({
            header: {
                id: status.name,
                content: <h1>{status.name}</h1>
            },
            items: state.tasks.filter(task => task.statusName === status.name).map(task => <TaskCard key={task.id} task={task}/>)
        }));

        return (
            <div className="app-root">
                <CommandPane/>
                <TaskTable columns={columns}/>
            </div>
        )
    }
}

export default App;