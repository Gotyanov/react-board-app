import React from 'react';
import Card from './Card';
import './TaskCard.css';

export default function TaskCard({task}){
    return (
        <div className="task-card">
            <Card>
                <article>
                    <h1>
                        <div className="task-id">{task.id}</div>{": "}
                        <div className="task-name">{task.name}</div>
                    </h1>
                    <div className="task-description" dangerouslySetInnerHTML={{__html: task.description}}></div>
                    <div className="task-assigner">{task.assigner}</div>
                </article>
            </Card>
        </div>
    );
}