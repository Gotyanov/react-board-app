import React, {Component} from 'react';
import './TaskTable.css';

export default class TaskTable extends Component {
    render() {
        
        let {columns} = this.props;
        let headers = [];
        let columnItems = [];

        for (let column of columns) {
            let {header, items} = column
            headers.push(header);
            columnItems.push(items);
        }

        let wrappedColumnItems = columnItems.map(cis => 
            <li className="column">
                <ul className="rows"> { cis.map(ci => <li>{ci}</li>) } </ul>
            </li>
        );

        return (
    <div className="task-table-container">
        <div className="absolute-container">
            <div className="scroll-x-container">
                <div className="absolute-x-container">
                    <div className="task-table">

                        <div className="headers-container">
                            <ul className="headers">{ headers.map(h => (<li key={h.id}>{h.content}</li>))} </ul>
                        </div>

                        <div className="columns-container">
                            <div className="absolute-container">
                                <div className="scroll-y-container">
                                    <div className="absolute-y-container">
                                        <ul className="columns">

                                            { wrappedColumnItems }

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        )
    }
}